# Validation tool for checking png conformance: #
* Build libpng solution: lpng1616\projects\vstudio\vstudio.sln
* conformance checker can be found in the build output: pngtest.exe

# To build pngfix tool: #
1. Build libpng: lpng1616\projects\vstudio\vstudio.sln
2. Build pngfix: lpng1616\projects\pngfix\pngfix\pngfix.sln (make sure to use the same build target as for libpng)
3. Copy libpng16.dll created in step 1 and zlib1.dll from the "libdll" folder into the build output of step 2

A pre-compiled version can already be found in the "PNG Conformance Stuff" folder!

# PNG conformance check / fix for the viewer (Requires ImageMagick): #
1. Run "iterate.bat" from the "PNG Conformance Stuff" folder and redirect output into a file (e.g. iterate c:\viewer\phoenix-firestorm-lgpl > result.log)
2. Run "PNGFixResultParser.exe" from the "PNG Conformance Stuff" (e.g. PNGFixResultParser result.log) - it creates two batch files "converter.bat" and "comparer.bat"
3. Run "converter.bat" - it will strip all comments from the PNG files
4. Run "comparer.bat" (comparer > null) - if it prints out only 0s, the conversion didn't change the picture data itself (picture still looks the same)
5. Run "iterate.bat" from the "PNG Conformance Stuff" folder and redirect output into a file (e.g. iterate c:\viewer\phoenix-firestorm-lgpl > result_new.log) and check the output file for "libpng FAILS test". If "libpng FAILS test" couldn't be found, all files are PNG conform

ImageMagick can be found in the Downloads section of this repository.