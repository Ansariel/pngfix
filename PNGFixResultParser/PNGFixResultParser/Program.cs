﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace PNGFixResultParser
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args.Length != 1)
            {
                Console.WriteLine("Please run with \"PNGFixResultParser <ResultFile>\"");
                return;
            }

            List<string> output = new List<string>();
            List<string> compareoutput = new List<string>();
            string[] lines = File.ReadAllLines(args[0]);

            for (int i = 0; i < lines.Length; ++i)
            {
                if (lines[i] == " libpng FAILS test")
                {
                    Console.WriteLine(lines[i - 1]);
                    output.Add("magick convert " + lines[i - 1] + " -strip " + lines[i - 1]);
                    compareoutput.Add("magick compare -metric AE " + lines[i - 1] + " " + lines[i - 1] + " diff.png");
                }
            }
            File.Delete("diff.png");

            File.WriteAllLines("converter.bat", output);
            File.WriteAllLines("comparer.bat", compareoutput);

        }
    }
}
